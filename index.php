<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/style.css">
  <title>parametros</title>
</head>
<?php

include "connect.php";

// $nasid = $_SESSION['nasid'];
// $realm = $_SESSION['realm'];
// $realm = $_SESSION['hotel'];
$nasid = "RD_Pardo200";
$realm = "@hotel200";
$hotel = "Hotel 200";

?>

<body>
  <div class="formulario">
    <h1>Area 51</h1>
    <form action="insert.php" method="POST">

      <div>
        <label for="hotel">Hotel</label>
	<?php print $hotel; ?>
        <input type=hidden name="hotel" id="hotel" type="text" value='<?php print $hotel; ?>' >
      </div>

      <div>
        <label for="nasid">NasId</label>
	<?php print $nasid; ?>
        <input type=hidden name="nasid" id="nasid" type="text" value='<?php print $nasid; ?>' >
      </div>
      
      <div>
        <label for="realm">Realm</label>
	<?php print $realm; ?>
        <input type=hidden name="Realm_n4g" id="realm" type="text" value='<?php print $realm; ?>' >
      </div>

      <div>
        <label for="integracao">Integração?</label>
        <input name="Integration" type="radio" value="true"  />Sim
        <input name="Integration" type="radio" value="false" checked="checked" />Não
      </div>

      <div>
        <label for="sistema">Sistema Hoteleiro</label>
        <select name="FrontDesk_System" id="">
          <option value="cm">CM</option>
          <option value="opera">Opera</option>
          <option value="engenho">Engenho</option>
          <option value="desbravador">Desbravador</option>
          <option value="app">APP</option>
          <option value="checkin">Checkin</option>
        </select>
      </div>
      
      <div>
        <label for="cobranca">Internet é cobrada?</label>
        <input name="Internet_Billing" type="radio" value="true" />Sim
        <input name="Internet_Billing" type="radio" value="false" checked="checked"  />Não
      </div>

      <div>
        <table border=1>
        <tr><td>Id</td><td>Profile</td><td>Value</td></tr>
        <?php
	    $query01="SELECT DISTINCT id,name FROM profiles WHERE available_to_siblings='1'";
	    $result01=$dbh->query($query01);
	    while ($row01 = $result01->fetch(PDO:: FETCH_ASSOC)) {
		print "<tr><td>". $row01['id']." </td><td>". $row01['name']." </td><td>";
		print "<input type=text name=".$row01['id']."value > </td></tr>";
	    }
	?>
        </table>
      </div>
      
      <div>
        <label for="confirma">Confirmação cobrança</label>
        <input name="Charge_Confirm" type="radio" value="true" />Sim
        <input name="Charge_Confirm" type="radio" value="false" checked="checked"  />Não
      </div>
      
      <div>
        <label for="perfildefault">Perfil de velocidade padrão</label>
        <?php
	    print "<SELECT name=perfildefault>";
	    $query02="SELECT DISTINCT id,name FROM profiles WHERE available_to_siblings='1'";
	    $result02=$dbh->query($query02);
	    while ($row02 = $result02->fetch(PDO:: FETCH_ASSOC)) {
		print "<option value=".$row02['id'].">".$row02['name']."</option>";
	    }
	    print "</SELECT>";
	?>
        
      </div>

      <div>
        <label for="telainicio">Tela inicial</label>
        <input name="Default_Screen" type="radio" value="room" checked="checked" />Apto
        <input name="Default_Screen" type="radio" value="voucher" />Voucher
      </div>

      <button type="submit">Enviar</button>

    </form>
  </div>
</body>

</html>